var pump_on = document.getElementById("pump_on_btn");
var pump_off = document.getElementById("pump_off_btn");
var set_temp = document.getElementById("set_temp");
var set_timer = document.getElementById("set_timer");
var heat_on = document.getElementById("heat_on_btn");
var heat_off = document.getElementById("heat_off_btn");
var stop_timer = document.getElementById("stop_timer_btn")
var heat_state: boolean;
var pump_state: boolean;

async function api(url: string): Promise<void> {
    const response = await fetch(url, {
        method: 'post',
    });
    if (!response.ok) {
        alert(await response.text());
    }
}
pump_on.addEventListener('click', function() {
    var state: String;
    if (pump_state) {
        state = "off"
    } else {
        state = "on"
    }
    api(`/grain/relay/pump/${state}`)
});

heat_on.addEventListener('click', function() {
    var state: String;
    if (heat_state) {
        state = "off"
    } else {
        state = "on"
    }
    api(`/grain/relay/heat/${state}`)
});


set_temp.addEventListener('click', function() {
    console.log("Sending temperature");
    var input_field: HTMLInputElement = document.getElementById("input_temp_field") as HTMLInputElement;
    var input: number = parseInt(input_field.value)

    if (input < 10 || input > 100) {
        alert(`Temperature must be between 10 and 100, got ${input}`)
        return
    }

    api(`/grain/set/temp/${input}`)
});

set_timer.addEventListener('click', function() {
    console.log("Sending timer");
    var input_field: HTMLInputElement = document.getElementById("input_timer_field") as HTMLInputElement;
    var input: number = parseInt(input_field.value)

    if (input < 0 || input > 255) {
        alert(`Timer cannot take values higher than 255 or lower than 0, got ${input}`)
        return
    }

    api(`/grain/set/timer/${input}`)
});

stop_timer.addEventListener('click', function() {
    api("/grain/timer/stop")
});

console.log("hello world!")
setInterval(function() {
    var xhr: XMLHttpRequest = new XMLHttpRequest();
    xhr.open("GET", "/grain/status", true);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onreadystatechange = function() {

        if (xhr.readyState === 4 && xhr.status === 200) {
            var json = JSON.parse(xhr.responseText);
            var minute: number = Number(json.timer.minute);
            var second: number = Number(json.timer.second);
            var hour: number = 0;
            var target: number = Number(json.temp.target);
            var current: number = Number(json.temp.current);
            var dev_name: string = json.name;
            var unit: string = 'C';

            if (minute > 60) {
                hour = Math.floor(minute / 60);
                minute = minute % 60;
            }
            var timer: string = String(hour).padStart(2, '0') + ":" + String(minute).padStart(2, '0') + ":" + String(second).padStart(2, '0');


            document.getElementById("timer_nums").innerHTML = timer;
            document.getElementById("temp_current").innerHTML = `${current}${unit}`;
            document.getElementById("dev_name").innerHTML = dev_name;
            document.getElementById("temp_target").innerHTML = `${target}${unit}`;
            heat_state = json.heat;
            pump_state = json.pump;

            if (pump_state) {
                pump_on.style.borderColor = "green";

            } else {
                pump_on.style.borderColor = "white";
            }

            if (heat_state) {
                heat_on.style.borderColor = "red";

            } else {
                heat_on.style.borderColor = "white";
            }
        }

    };

    xhr.send();
}, 1000);
