
Control your GrainFather using a web browser!

This is made specifically for a **Grain Connect Control Box**, and to be as
simple as possible. It supports turning on/off heater and pump, and setting
timer in minutes, and temperature in celsius. It will also continuously update
based on the state from the device itself, this means there shouldn't be any
problem from changing state on the device vs on the website.

# Usage

## Dependencies:

Requires the following programs:

- rust, nightly
- make
- rust-libdbus
  - Ubuntu package: librust-libdbus-sys-dev
  - Fedora package: rust-libdbus-sys-devel

Run it on a computer within Bluetooth range of the GrainFather, like a Raspberry
Pi. It'll look for a Bluetooth device with the MAC address specified in
`main::main()`.

``` sh
make scripts
cargo run --release
```

You will now be able to reach the web ui from the IP address of the device that
runs it, on port 8000.

**note:** The above commands will compile the software the first time it is ran,
and might take a while on slow computers such as a Raspberry Pi.

# Screenshot

<img src="/screenshot/screenshot_oneplus_all_off.jpg"  width="200">
<img src="/screenshot/screenshot_oneplus_all_on.jpg"  width="200">
