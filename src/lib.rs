#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate serde_derive;
extern crate serde_json;

pub mod grain;

#[derive(Serialize, Clone, Default)]
pub struct Temp {
    pub target: f32,
    pub current: f32,
}

impl Temp {
    pub fn from_grain(temp: &str) -> Result<Temp, std::num::ParseFloatError> {
        let data: Vec<&str> = temp.split::<char>(',').collect();

        Ok(Temp {
            target: data[0].chars().skip(1).collect::<String>().parse::<f32>()?,
            current: data[1].parse::<f32>()?,
        })
    }
}

#[derive(Debug, Clone, Serialize, Default)]
pub struct Time {
    pub minute: u8,
    pub second: u8,
    pub active: bool,
    pub initial_minute: u8,
}

impl Time {
    pub fn from_grain(time: &str) -> Result<Time, std::num::ParseIntError> {
        let data: Vec<&str> = time.split::<char>(',').collect();

        Ok(Time {
            minute: data[1].parse::<u8>()?.checked_sub(1).unwrap_or(0),
            second: data[3].parse::<u8>()?,
            active: if time.chars().nth(1).unwrap_or('0') == '1' {
                true
            } else {
                false
            },
            initial_minute: data[2].parse::<u8>()?,
        })
    }
    pub fn new() -> Time {
        Time {
            minute: 0,
            second: 0,
            active: false,
            initial_minute: 0,
        }
    }
}

impl std::fmt::Display for Time {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.minute > 60 {
            write!(
                f,
                "{:02}:{:02}:{:02}",
                self.minute / 60,
                self.minute % 60,
                self.second
            )
        } else {
            write!(f, "{:02}:{:02}", self.minute, self.second)
        }
    }
}
