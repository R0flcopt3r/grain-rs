#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;

extern crate btleplug;
extern crate rand;

use btleplug::api::{BDAddr, Central};
use btleplug::bluez::manager::Manager;
use grain_rs::grain::Status;
use std::str::FromStr;
use std::sync::mpsc::{sync_channel, Receiver, SyncSender};
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;

mod web;

pub fn main() {
    let (sync_sender, receiver): (SyncSender<String>, Receiver<String>) = sync_channel(2);
    let grain_status = Arc::new(Mutex::new(Status::default()));

    let mac: BDAddr = BDAddr::from_str("BB:A0:50:0E:1A:06").unwrap();

    let adapter = Manager::new()
        .unwrap()
        .adapters()
        .unwrap()
        .into_iter()
        .nth(0)
        .expect("Unable to find adapter");

    adapter.start_scan().unwrap();
    thread::sleep(Duration::from_secs(2));

    let web_mutex = grain_status.clone();
    let web_sender = sync_sender.clone();
    let web = thread::spawn(move || web::web(web_mutex, web_sender).launch());

    let btle_mutex = grain_status.clone();
    let btle = thread::spawn(move || {
        grain_rs::grain::connect(btle_mutex, receiver, mac, adapter);
    });

    btle.join().unwrap();
    web.join().unwrap();
}
