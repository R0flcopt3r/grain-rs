extern crate serde_json;
use grain_rs::{grain, grain::Status};
use rocket::{
    fairing::AdHoc,
    http,
    response::{
        content::{JavaScript, Json},
        status, NamedFile,
    },
    State,
};
use rocket_contrib::templates::Template;
use std::{
    path::{Path, PathBuf},
    sync::{mpsc::SyncSender, Arc, Mutex},
};

#[get("/")]
fn hello(state: State<Arc<Mutex<Status>>>) -> Template {
    let context = &(*state.lock().unwrap());
    Template::render("index", &context)
}

#[get("/grain/status")]
fn get_data(state: State<Arc<Mutex<Status>>>) -> Json<Status> {
    Json((*state.lock().unwrap()).clone())
}

struct AssetDir(String);
struct ScriptDir(String);

#[get("/script/<script..>")]
fn get_script(script: PathBuf, script_dir: State<ScriptDir>) -> Option<JavaScript<NamedFile>> {
    match NamedFile::open(Path::new(&script_dir.0).join(&script)).ok() {
        Some(file) => Some(JavaScript(file)),
        None => None,
    }
}

#[get("/asset/<asset..>")]
fn get_asset(asset: PathBuf, asset_dir: State<AssetDir>) -> Option<NamedFile> {
    NamedFile::open(Path::new(&asset_dir.0).join(&asset)).ok()
}

#[post("/grain/relay/<dev>/<state>")]
fn dev_state(
    dev: String,
    state: String,
    cmds: State<SyncSender<String>>,
) -> rocket::response::status::Custom<String> {
    let command = match (dev.as_str(), state.as_str()) {
        ("pump", "on") => grain::TURN_ON_PUMP,
        ("pump", "off") => grain::TURN_OFF_PUMP,
        ("heat", "on") => grain::TURN_ON_HEAT,
        ("heat", "off") => grain::TURN_OFF_HEAT,
        (_, _) => return status::Custom(
            http::Status::BadRequest,
            format!(
                "'{state}' is not a valid state for device '{dev}' or device '{dev}' does not exist.",
                state = state,
                dev = dev
            ),
        ),
    };

    if cmds.send(String::from(command)).is_err() {
        return status::Custom(
            http::Status::BadRequest,
            String::from("Error sending command."),
        );
    }

    status::Custom(http::Status::Ok, String::from(""))
}

#[post("/grain/set/<dev>/<num>")]
fn set_dev(dev: String, num: u8, cmds: State<SyncSender<String>>) -> status::Custom<String> {
    let command = match dev.as_str() {
        "timer" => format!("S{:18}", num),
        "temp" => format!("${:18}", num),
        _ => return status::Custom(http::Status::NotFound, format!("device not found: {}", dev)),
    };

    if cmds.send(command).is_err() {
        return status::Custom(
            http::Status::BadRequest,
            String::from("Error sending command."),
        );
    }

    status::Custom(http::Status::Ok, String::from(""))
}

#[post("/grain/timer/stop")]
fn stop_timer(cmds: State<SyncSender<String>>) {
    cmds.send(String::from(grain::CANCEL_TIMER)).unwrap();
}

#[post("/grain/disconnect")]
fn disconnect(cmds: State<SyncSender<&'static str>>) {
    cmds.send(grain::DISCONNECT_AND_CANCEL_SESSION).unwrap();
}

pub fn web(mutex: Arc<Mutex<Status>>, cmds: SyncSender<String>) -> rocket::Rocket {
    rocket::ignite()
        .mount(
            "/",
            routes![
                hello, get_data, get_script, get_asset, disconnect, set_dev, stop_timer, dev_state
            ],
        )
        .manage(mutex)
        .manage(cmds)
        .attach(AdHoc::on_attach("Script Config", |rocket| {
            let script_dir = rocket
                .config()
                .get_str("script_dir")
                .unwrap_or("scripts/")
                .to_string();
            Ok(rocket.manage(ScriptDir(script_dir)))
        }))
        .attach(AdHoc::on_attach("Asset Config", |rocket| {
            let asset_dir = rocket
                .config()
                .get_str("asset_dir")
                .unwrap_or("asset/")
                .to_string();
            Ok(rocket.manage(AssetDir(asset_dir)))
        }))
        .attach(Template::fairing())
}
