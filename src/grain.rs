use crate::Temp;
use crate::Time;

use btleplug::api::BDAddr;
use btleplug::api::{Central, NotificationHandler, Peripheral};
use btleplug::bluez::adapter::Adapter;

use rocket::http::ContentType;
use rocket::response::{self, Responder, Response};

use std::io::Cursor;
use std::sync::mpsc::Receiver;
use std::sync::Arc;
use std::sync::Mutex;

pub const DISMISS_BOIL_ADDITION_ALERT: &str = "A";
pub const CANCEL_TIMER: &str = "C";
pub const DECREMENT_TARGET_TEMP: &str = "D";
pub const CANCEL_OR_FINISH_SESSION: &str = "F";
pub const PAUSE_OR_RESUME_TIMER: &str = "G";
pub const TOGGLE_HEAT: &str = "H";
pub const INTERACTION_COMPLETE: &str = "I";
pub const TURN_OFF_HEAT: &str = "K0";
pub const TURN_ON_HEAT: &str = "K1";
pub const TURN_OFF_PUMP: &str = "L0";
pub const TURN_ON_PUMP: &str = "L1";
pub const GET_CURRENT_BOIL_TEMP: &str = "M";
pub const TOGGLE_PUMP: &str = "P";
pub const DISCONNECT_MANUAL_MODE_NO_ACTION: &str = "Q0";
pub const DISCONNECT_AND_CANCEL_SESSION: &str = "Q1";
pub const DISCONNECT_AUTO_MODE_NO_ACTION: &str = "Q2";
pub const PRESS_SET: &str = "T";
pub const INCREMENT_TARGET_TEMP: &str = "U";
pub const DISABLE_SPARGE_WATER_ALERT: &str = "V";
pub const GET_FIRMWARE_VERSION: &str = "X";
pub const RESET_CONTROLLER: &str = "Z";
pub const RESET_RECIPE_INTERRUPTED: &str = "!";
pub const TURN_OFF_SPARGE_COUNTER_MODE: &str = "d0";
pub const TURN_ON_SPARGE_COUNTER_MODE: &str = "d1";
pub const TURN_OFF_BOIL_CONTROL_MODE: &str = "e0";
pub const TURN_ON_BOIL_CONTROL_MODE: &str = "e1";
pub const EXIT_MANUAL_POWER_CONTROL_MODE: &str = "f0";
pub const ENTER_MANUAL_POWER_CONTROL_MODE: &str = "f1";
pub const GET_CONTROLLER_VOLTAGE_AND_UNITS: &str = "g";
pub const TURN_OFF_SPARGE_ALERT_MODE: &str = "h0";
pub const TURN_ON_SPARGE_ALERT_MODE: &str = "h1";

pub const SERVICE_ID: uuid::Uuid = uuid::Uuid::from_u128(0x0000cdd0_0000_1000_8000_00805f9b34fb);
pub const WRITE_ID: uuid::Uuid = uuid::Uuid::from_u128(0x0003cdd2_0000_1000_8000_00805f9b0131);
pub const NOTIFY_ID: uuid::Uuid = uuid::Uuid::from_u128(0x0003CDD1_0000_1000_8000_00805F9B0131);

#[derive(Clone, Debug)]
pub struct Grain<P: Peripheral> {
    peripheral: P,
    write: btleplug::api::Characteristic,
    service: btleplug::api::Characteristic,
    notify: btleplug::api::Characteristic,
}

impl<P: Peripheral> Grain<P> {
    fn send_no_reponse(&self, msg: &str) -> Result<(), btleplug::Error> {
        self.peripheral.write(
            &self.write,
            &format!("{:19}", &msg).into_bytes(),
            btleplug::api::WriteType::WithoutResponse,
        )
    }

    pub fn disconnect(&self) -> Result<(), btleplug::Error> {
        self.peripheral.unsubscribe(&self.notify)?;
        self.peripheral.disconnect()
    }

    pub fn connect(&self) -> Result<(), btleplug::Error> {
        self.peripheral.connect()
    }

    pub fn set_temp(&self, temp: u8) -> Result<(), btleplug::Error> {
        self.send_no_reponse(format!("${}", temp).as_str())
    }

    pub fn set_timer(&self, minutes: u8) -> Result<(), btleplug::Error> {
        self.send_no_reponse(format!("S{}", minutes).as_str())
    }

    pub fn send_cmd(&self, cmd: String) -> Result<(), btleplug::Error> {
        self.send_no_reponse(cmd.as_str())
    }

    pub fn subscribe(
        &self,
        characteristic: btleplug::api::Characteristic,
    ) -> Result<(), btleplug::Error> {
        self.peripheral.subscribe(&characteristic)
    }

    pub fn on_notification(&self, handler: NotificationHandler) -> Result<(), btleplug::Error> {
        self.peripheral.on_notification(handler);
        self.peripheral.subscribe(&self.notify)?;
        Ok(())
    }

    pub fn try_new(peripheral: P) -> Result<Grain<P>, btleplug::Error> {
        peripheral.connect()?;
        peripheral.discover_characteristics()?;

        let grain = Grain {
            write: peripheral
                .characteristics()
                .iter()
                .find(|c| c.uuid == WRITE_ID)
                .ok_or(btleplug::Error::NotSupported(
                    "Write characteristic not found".to_string(),
                ))?
                .clone(),
            service: peripheral
                .characteristics()
                .iter()
                .find(|c| c.uuid == SERVICE_ID)
                .ok_or(btleplug::Error::NotSupported(
                    "Service characteristic not found".to_string(),
                ))?
                .clone(),
            notify: peripheral
                .characteristics()
                .iter()
                .find(|c| c.uuid == NOTIFY_ID)
                .ok_or(btleplug::Error::NotSupported(
                    "Notify characteristic not found".to_string(),
                ))?
                .clone(),
            peripheral: peripheral,
        };

        Ok(grain)
    }
}

pub fn connect(status: Arc<Mutex<Status>>, cmds: Receiver<String>, mac: BDAddr, adapter: Adapter) {
    let device = adapter
        .peripherals()
        .into_iter()
        .find(|p| p.properties().address == mac)
        .expect(format!("No device found with MAC: {:?}", &mac).as_str());

    if device.is_connected() {
        device.disconnect().unwrap();
    }

    let name: String = device
        .properties()
        .local_name
        .unwrap_or(device.properties().address.to_string())
        + ":"
        + &device
            .properties()
            .address
            .to_string()
            .split(":")
            .last()
            .unwrap()
            .to_string();

    (*status.lock().unwrap()).name = name;

    let grain = Grain::try_new(device).unwrap();

    grain
        .on_notification(Box::new(
            move |notification: btleplug::api::ValueNotification| {
                if notification.value.len() == 17 {
                    let test = std::str::from_utf8(&notification.value).unwrap_or("_");

                    let data = test.split(",").collect::<Vec<&str>>();
                    let desc = &data[0].chars().nth(0).unwrap_or('0');
                    let mut shipment = status.lock().unwrap();

                    match &desc {
                        'X' => {
                            // Temperature
                            (*shipment).temp = Temp::from_grain(&test).unwrap();
                        }
                        'T' => {
                            // Timer
                            (*shipment).timer = Time::from_grain(&test).unwrap();
                        }
                        'Y' => {
                            // pump and heat status
                            (*shipment).pump = data[1].chars().nth(0).unwrap_or('0') == '1';

                            (*shipment).heat = data[0].chars().nth(1).unwrap_or('0') == '1';
                        }
                        _ => return,
                    }
                }
            },
        ))
        .unwrap();

    loop {
        grain.send_cmd(cmds.recv().unwrap()).unwrap();
    }
}

#[derive(Serialize, Clone, Default)]
pub struct Status {
    pub name: String,
    pub timer: Time,
    pub temp: Temp,
    pub pump: bool,
    pub heat: bool,
}

impl<'a> Responder<'a> for Status {
    fn respond_to(self, _: &rocket::Request) -> response::Result<'a> {
        Response::build()
            .header(ContentType::JSON)
            .sized_body(Cursor::new(serde_json::to_string(&self.clone()).unwrap()))
            .ok()
    }
}
