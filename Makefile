##
# Grainfather
#
# @file
# @version 0.1

.PHONY: cargo_run, scripts clean_js script/dist/tsc.js

run: scripts/dist/timer.js cargo_run

cargo_run:
	cargo run

scripts:
	node_modules/.bin/tsc --build tsconfig.json

clean_js:
	rm -rf scripts/dist

# end
